**Business Case for <Project Name>**

**Date**

**Prepared by:**

| **1.0 Introduction/ Background**  |---|
| **2.0 Business Objective**     |---|
| **3.0 Current Situation and Problem/Opportunity Statement**    |---|
| **4.0 Critical Assumption and Constraints**   . |---|
| **5.0 Analysis of Option and Recommendation**     |---|
| **6.0 Preliminary Project Requirements**     |---|
| **7.0 Budget Estimate and Financial Analysis**     |---|
| **8.0 Schedule Estimate**     |---|
| **9.0 Potential Risks**     |---|
| **10.0 Exhibits** Exhibit A: Financial Analysis (Insert image here) | --- |